package cn.itlaobing;

import cn.itlaobing.service.HelloService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        //实例化容器，加载配置文件
        ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
        //到容器中取出bean对象  getBean方法后面的参数是在 xml文件中定义的id
        HelloService helloService=(HelloService)ctx.getBean("helloService");
        System.out.println(helloService.sayHello("张三"));
    }
}
